package ru.t1.skasabov.tm.api.service;

import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    void add(Project project);

    void changeProjectStatusById(String id, Status status);

    void changeProjectStatusByIndex(Integer index, Status status);

    Project create(String name, String description);

    Project create(String name);

    void updateById(String id, String name, String description);

    void updateByIndex(Integer index, String name, String description);

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    List<Project> findAll(Sort sort);

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void remove(Project project);

}
