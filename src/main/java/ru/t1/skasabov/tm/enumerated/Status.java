package ru.t1.skasabov.tm.enumerated;

import ru.t1.skasabov.tm.exception.field.StatusEmptyException;
import ru.t1.skasabov.tm.exception.field.StatusIncorrectException;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    public static String toName(final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    public static Status toStatus(final String value) {
        if (value == null || value.isEmpty()) throw new StatusEmptyException();
        for (final Status status: values()) {
            if (status.name().equals(value)) return status;
        }
        throw new StatusIncorrectException();
    }

    private final String displayName;

    Status(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
